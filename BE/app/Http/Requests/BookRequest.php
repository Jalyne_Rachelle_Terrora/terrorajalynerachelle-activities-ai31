<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'required|max:255',
            'author' => 'required|max:255',
            'category_id' => 'required|integer',
            'copies' => 'exists:patrons,id'

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

            'name.required' => 'Please enter name.',
            'name.max' => 'Name must be a maximum of 255 characters.',

            'author.required' => 'Please enter author.',
            'author.max' => 'Author must be a maximum of 255 characters.',

            'copies.required' => 'Please enter number of copies.',
            'copies.integer' => 'Copies must be an integer.',

        ];
    }

    public function failedValidation(Validator $validator){

        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }

}

