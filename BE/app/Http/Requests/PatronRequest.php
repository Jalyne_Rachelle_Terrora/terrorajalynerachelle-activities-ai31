<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class PatronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'last_name' => 'bail|required|min:6|max:255',
            'first_name' => 'bail|required|min:3|max:255',
            'middle_name' => 'bail|required|min:6|max:255',
            'email' => 'bail|required|email|unique:patrons'

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

            'first_name.required' => 'Please enter first name.',
            'first_name.min' => 'First name must be a minimum of 3 characters.',
            'first_name.max' => 'First name must be a maximum of 255 characters.',

            'last_name.required' => 'Please enter last name.',
            'last_name.min' => 'Last name must be a minimum of 6 characters.',
            'last_name.max' => 'Last name must be a maximum of 255 characters.',

            'middle_name.required' => 'Please enter middle name.',
            'middle_name.min' => 'Middle name must be a minimum of 6 characters.',
            'middle_name.max' => 'Middle name must be a maximum of 255 characters.',

            'email.required' => 'Please enter email.',
            'email.email' => 'Email address must be valid.',
            'email.unique' => 'The same email address already exist in the database.'
        ];
    }

    public function failedValidation(Validator $validator){

        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
