<?php

namespace App\Http\Requests;

use App\Models\Book;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class BorrowedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $book = Book::find(request()->get('book_id'));

        if(!empty($book)) {
                    
            $copies = $book->copies;
        } else {
            
            $copies = request()->get('copies');
        }
        
        return [

            'copies' => ['required',"lte: {$copies}", 'bail', 'gt:0'],
            'book_id' => 'bail|required|exists:books,id',
            'patron_id' => 'exists:patrons,id'

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

            'book_id.exists' => 'Book cannot be found in the database.',
            'copies.lte' => 'The borrowed copies entered exceeded the total copies of books.',
            'patron_id.exists' => 'Patron cannot be found in the database'

        ];
    }

    public function failedValidation(Validator $validator){

        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
