<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use Illuminate\Http\Request;
use App\Http\Requests\ReeturnedBookRequest;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(RetrunedBook::with(['book','patron', 'categories'])->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrowed_book = BorrowedBook::where([
            ['book_id', $request->book_id],
            ['patron_id', $request->patron_id],

        ])->firstOrFail();
            if(!empty($borrowed_book))
        {
            if ($borrowed_book->copies == $request->copies){
                $borrowed_book->delete();
            }
            else
            {
                $borrowed_book->update(['copies' => $borrowed_book->copies - $request->copies]);
            }

            $create_returned = ReturnedBook::create($request->only(['book_id', 'copies', 'patron_id']));
            $returned_book = ReturnedBook::with(['book'])->find($create_returned->id);
            $copies = $returned_book->book->copies + $request->copies;

            $returned_book->book->update(['copies' => $copies]);

            return response()->json(['message' => 'Book returned successfully!']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
