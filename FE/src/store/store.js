import Vue from "vue";
import Vuex from "vuex";
import Books from "./modules/Books";
import Patrons from "./modules/Patrons";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: [Books, Patrons],
});