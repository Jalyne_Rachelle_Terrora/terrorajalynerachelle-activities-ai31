import Vue from "vue";
import VueRouter from "vue-router";
import dashboard from "../pages/dashboard.vue";
import patron from "../pages/patron.vue";
import book from "../pages/book.vue";
import setting from "../pages/setting.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: dashboard,
    },
    {
      path: "/dashboard",
      name: "Dashboard",
      component: dashboard,
    },
    {
      path: "/patron",
      name: "Patron",
      component: patron,
    },

   {
      path: "/book",
      name: "Book",
      component: book,
    },
    
    {
      path: "/setting",
      name: "Settings",
      component: setting,
    },
    
  ],
});
